package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyService {

    @NotNull
    private final Properties properties = new Properties();
    @NotNull
    private final String PATH_TO_PROPERTIES = "src/main/resources/application.properties";

    public PropertyService() {
        try {
            @NotNull final FileInputStream fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    public final String getSalt() {
        return properties.getProperty("salt");
    }

    @NotNull
    public final Integer getCycle() {
        return Integer.parseInt(properties.getProperty("cycle"));
    }

    @Nullable
    public final String getUrl() {
        return properties.getProperty("url");
    }

    @Nullable
    public final String getDriver() {
        return properties.getProperty("driver");
    }

    @Nullable
    public final String getDbType() {
        return properties.getProperty("dbType");
    }

    @Nullable
    public final String getDbHost() {
        return properties.getProperty("dbHost");
    }

    @Nullable
    public final String getDbPort() {
        return properties.getProperty("dbPort");
    }

    @Nullable
    public final String getDbName() {
        return properties.getProperty("dbName");
    }

    @Nullable
    public final String getDbLogin() {
        return properties.getProperty("dbLogin");
    }

    @Nullable
    public final String getDbPassword() {
        return properties.getProperty("dbPassword");
    }

}
