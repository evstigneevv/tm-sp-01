<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 13.02.2020
  Time: 12:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Project id</th>
        <th>Task id</th>
        <th>Task name</th>
        <th>Task description</th>
        <th>Task date of creation</th>
        <th>Task start date</th>
        <th>Task finish date</th>
        <th>Task status</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><c:out value="${task.project.id}"/></td>
        <td><c:out value="${task.id}"/></td>
        <td><c:out value="${task.name}"/></td>
        <td><c:out value="${task.description}"/></td>
        <td><c:out value="${task.dateOfCreation}"/></td>
        <td><c:out value="${task.dateStart}"/></td>
        <td><c:out value="${task.dateFinish}"/></td>
        <td><c:out value="${task.status}"/></td>
        <td><a href="${pageContext.request.contextPath}/taskEdit/${task.id}">Edit</a></td>
        <td><a href="${pageContext.request.contextPath}/taskDelete/${task.id}">Remove</a></td>
    </tr>
    </tbody>
</table>
</body>
</html>