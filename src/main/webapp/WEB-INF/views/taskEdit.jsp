<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edit project</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<form:form method="post" action="${pageContext.request.contextPath}/taskEdit" modelAttribute="task">
    <table>
        <tr>
            <td></td>
            <td><form:hidden path="id"/></td>
        </tr>
        <tr>
            <td>Name :</td>
            <td><form:input path="name"/></td>
        </tr>
        <tr>
            <td>description :</td>
            <td><form:input path="description"/></td>
        </tr>
        <tr>
            <td>Start date :</td>
            <td><form:input path="dateStart"/></td>
        </tr>
        <tr>
            <td>Finish date :</td>
            <td><form:input path="dateFinish"/></td>
        </tr>
        <tr>
            <td>Status :</td>
            <td><form:select path="status" items="${status}"/></td>
        </tr>
    </table>
    <button type="submit" class="button">Save edit</button>
</form:form>
</body>
</html>