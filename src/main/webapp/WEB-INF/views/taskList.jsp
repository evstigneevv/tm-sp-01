<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Task List</title>
</head>
<%@include file="navbar.jsp" %>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Project id</th>
        <th>Task id</th>
        <th>Task name</th>
        <th>Task description</th>
        <th>View</th>
        <th>Edit</th>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td><c:out value="${task.project.id}"/></td>
            <td><c:out value="${task.id}"/></td>
            <td><c:out value="${task.name}"/></td>
            <td><c:out value="${task.description}"/></td>
            <td><a href="${pageContext.request.contextPath}/taskView/${task.id}">View</a></td>
            <td><a href="${pageContext.request.contextPath}/taskEdit/${task.id}">Edit</a></td>
            <td><a href="${pageContext.request.contextPath}/taskDelete/${task.id}">Remove</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/taskCreate/${id}" role="button">Create task</a>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/taskList/${id}" role="button">Refresh</a>
</body>
</html>