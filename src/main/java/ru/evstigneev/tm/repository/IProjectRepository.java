package ru.evstigneev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Date;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    @Modifying
    @Query("UPDATE Project SET name = :name, description = :description, dateStart = :dateStart, dateFinish = " +
            ":dateFinish, status = :status WHERE id = :id")
    void update(@Param("id") final String id, @Param("name") final String name, @Param("description") final String description,
                @Param("dateStart") final Date dateStart, @Param("dateFinish") final Date dateFinish,
                @Param("status") final Status status);

}
